package com.example.springdatajpa.services;

import com.example.springdatajpa.entity.User;
import com.example.springdatajpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServices {
    @Autowired
    UserRepository userRepository;

    public User getUserById(long id) {
        return userRepository.findById(id).get();
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        userRepository.findAll().forEach(user -> users.add(user));
        return users;
    }

    public void createUser(User user) {
        userRepository.save(user);
    }

    public void updateUser(User users, Long id) {
        userRepository.findById(id).map(user -> {
            user.setAge(users.getAge());
            user.setName(users.getName());
            user.setEmail(users.getEmail());
            return userRepository.save(user);
        }).orElseGet(() -> {return userRepository.save(users);});
    }

    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }
}
