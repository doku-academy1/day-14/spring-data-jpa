package com.example.springdatajpa.services;

import com.example.springdatajpa.entity.Order;
import com.example.springdatajpa.repository.OrderRespository;
import com.example.springdatajpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServices {

    @Autowired
    UserRepository userRepository;
    @Autowired
    OrderRespository orderRespository;

    public List<Order> getOrderByUserId(Long user_id) {
        List<Order> orders = orderRespository.findByUserId(user_id);
        return orders;
    }

    public List<Order> getAllOrders() {
        List<Order> listOrder = new ArrayList<>();
        orderRespository.findAll().forEach(order -> {listOrder.add(order);});
        return listOrder;
    }

    public Order createOrder(Long user_id, Order order) {
        return userRepository.findById(user_id).map(user -> {
            order.setUser(user);
//            Order newOrder = new Order(order.getId(), order.getPrice(), order.getName_product(), order.getQuantity());
            return orderRespository.save(order);
        }).orElseThrow();
    }

    public void deleteOrderById(Long user_id, Long order_id) {
        orderRespository.findByIdAndUserId(user_id, order_id).map(order ->
        {orderRespository.delete(order);
        return ResponseEntity.ok().build();
        }
        );
    }

    public Order updateOrderById(Long user_id, Long order_id, Order orders) {
        if (!userRepository.existsById(user_id)) {
            System.out.println("cannot find Id!");
        }
        return orderRespository.findByIdAndUserId(user_id, order_id).map(order -> {
            order.setName_product(orders.getName_product());
            order.setPrice(orders.getPrice());
            order.setQuantity(orders.getQuantity());
            return orderRespository.save(order);
        }).orElseGet(() -> {return orderRespository.save(orders);});
    }
}
